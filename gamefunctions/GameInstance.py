#Game Instance
#   - Child of GameSupervisor
#   -cache latest stats / game details

from BaseCache import BaseCache
import cPickle as pickle
USER_IDLE_TIME=300

from gamefunctions import ChannelManager
from collections import OrderedDict
from fantasycoached import constants

class Game(BaseCache):
	#chat=[]  - Realized that it would copy
	leaderboard=dict({})
	def __init__(self, raw_data, app_dict):
		super(Game,self).__init__()
		# Create model and increment game counter
		self.debug('GI.__init__ -Game instance class launched ..')
		if not 'id' in raw_data :
			self.debug('GI.init - no id field in gamedict : %s' %raw_data)
			raise Exception("GI.init - Wrong input : id is required ")
		#if 'id' not in  raw_data or 'nfl_match_id' not in raw_data:
		#	raise Exception("Wrong input : id and nfl_match_id is require ")
		self.chat=[]
		self.app_name = "fcgame"
		self.model = "UserGame"
		game_id = raw_data['id']
		self.r_id = game_id
		self.app_dict = app_dict # so we can access the supervisors
		self.load_from_db()
		raw_data = self.r_dict
		self.match_id =  raw_data['nfl_match_id'].pk
		try:
			_t = self.app_dict['MatchSupervisor'].match_obj(self.match_id)
			self.debug('GI.init - Checking MS to see if game exists, status: %s' %(_t))
		except Exception as e:
			self.debug('GI.init - Match Supervisor does not appear to have match created for id: %s' %(self.match_id))
			self.error(e)
		self.r_key = 'M%sGame%s' %(self.match_id, game_id)
		match_games = 'NFLM%s' %self.match_id #Sandeep: Changed key to same as match key.
		self.r_dict.update({'ActiveMembers':{}})
		self.r_dict.update({'Quarters_played':[]})
		self.channel = None

		if self.r.exists(self.r_key):
			self.debug('GI.init - Game exists in match_game key - adding to dict')
			m_dict = self.unserialize(self.r.hget(match_games, 'data'))
			#m_dict['Games'] = set(m_dict['games'].append(game_id))
			m_dict['games'].append(game_id)
			m_dict['games_count']=len(m_dict['games'])
			self.debug('GI.init - Game exists - added successfully')
			self.r.hset(match_games, 'data', self.serialize(m_dict))
		else:
			self.debug('GI.init - Game did not exist in match_games - loading from db')
			self.load_from_db()
			#self.r.hset(self.r_key, 'data', self.serialize(self.r_dict))
			# JT 1/1 - above done by redis_init
			self.redis_init()
		# set game properties
		if 'added_date' not in raw_data:
			self.app_dict['GameSupervisor'].gamedict['added_date']=self.sys_time()
		#if our match is active
		if self.app_dict['MatchSupervisor'].match_is_active(self.match_id):
			self.debug('GI.init - Match is active')
			print self.r_dict
			self.start_game()	
		else:
			self.debug('GI.init - Match not active - did not start game')
		self.debug('GI.init - Completed successfully')

		#End init
	def get_match_id(self):
		return self.match_id 

	
	def add_member(self, UserID):
		self.debug('GI.add_member - Attempting to add member : %s' %UserID)
		#check if user alredy in game 
		
		if (UserID in self.r_dict['members']):
			self.debug('GI.add_member - user already in game - not adding')
			return False
		else:
			members = self.r_dict['members'][:]
			self.debug('GI.add_member - 0now members %s'%members)
			members.append(UserID)
			self.debug('GI.add_member - -1now members %s'%members)
			self.r_dict['members'] = members
			#probably need to call Scorer.register_user here
			self.debug('GI.add_member - now members %s'%self.r_dict['members'])
			if self.app_dict['MatchSupervisor'].match_is_active(self.match_id):
				self.app_dict['UserSupervisor'].add_user({'id':int(UserID)})
				self.debug('GI.add_member - 1now members %s'%self.r_dict['members'])
				self.scorer.register_user(self.r_id, UserID)
				self.debug('GI.add_member - 2now members %s'%self.r_dict['members'])
				self.update_ts()
				self.redis_sync() #needed to add new member to redis
				self.update_leaderboard()
			self.debug('GI.add_member - 3now members %s'%self.r_dict['members'])
			self.update_ts()
			self.debug('GI.add_member - 4now members %s'%self.r_dict['members'])
			self.redis_sync()
			self.debug('GI.add_member - 5now members %s'%self.r_dict['members'])
			return True

	def get_leaderboard(self):
		self.sync_if_required()
		return self.leaderboard

	def update_leaderboard(self):
		self.debug('GI.update_leaderboard - Triggered')
		self.debug("GI.update_leaderboard - Game id %s"%self.r_id)
		self.debug("GI.update_leaderboard - Members  %s"%self.r_dict['members'])

		play_type=self.r_dict['gameplay_type']
		try:
			self.redis_sync()
			if self.r_dict['members'] and len(self.r_dict['members'])>0:
				t_leaderboard={}
				for user in self.r_dict['members']:
					u_res=self.scorer.get_user_stats(user, self.r_id)
					t_leaderboard.update({user:u_res})
					self.can_user_play_game(user)
				self.rank_leaderboard(t_leaderboard)
				# Broadcast message for upload leaderboard.
				channel = self.get_channel()
				leaderboard_update = {'leaderboard':'updated'}
				self.app_dict['ChannelSupervisor'].broadcast_message(channel,leaderboard_update)
				self.update_ts()
				self.redis_sync()
				#send over socket - self.leaderboard
			else:
				self.debug('GI.update_leaderboard - no members to update')
			return True
		except Exception as e:
			self.error('GI.update_leaderboard - likely no members key')
			self.error(e)
			return False

	def rank_leaderboard(self, lead):
		try:
			self.info("GI.rank_leaderboard - Triggered")
			self.info(lead)
			flag='Agg_Score'
			new_board = OrderedDict(sorted(lead.iteritems(), key=lambda x: x[1][flag],  reverse=True))
			self.info("GI :: New Board %s"%new_board)
			self.leaderboard=new_board
		except Exception as e:
			self.error('GI.rank_leaderboard - failed ')
			self.error(e)

	def add_chat(self, item):
		#works if chat has uuid - otherwise change
		self.chat.append(item)

	def get_chat(self,num):
		return self.chat[-num:]

	def set_channel(self):
		self.debug('GI.set_channel - Triggered')
		if not self.channel:
			channel_key = ChannelManager.occupy_channel()
			self.channel = channel_key
			if not channel_key:
				self.error("GI.set_channel - No channel found for game %s" %self.r_id)


	def remove_channel(self):
		self.debug('GI.remove_channel - Triggered')
		channel_key = self.app_dict['ChannelSupervisor'].release_channel(self.channel)
		self.channel = None

	def get_channel(self):
		return self.channel   

	def start_game(self):
		self.debug('GI.start_game - Triggered')
		self.register_scorer()
		self.load_member()
		#not working because there are no users defined in the game or match
		self.scorer.register_game(self.r_dict)
		self.set_channel()
		self.r_dict.update({'active':True})
		self.update_ts()
		self.redis_sync()
		#register channel
		#start leaderboard
		return True

	def load_member(self):
		self.info("GI.load_member() - loading members %s"%self.r_dict['members'])
		#import pdb
		#pdb.set_trace()
		if self.r_dict['members']:
			for user_id in self.r_dict['members']:
			   self.app_dict['UserSupervisor'].add_user({'id':int(user_id)})
			   self.scorer.register_user(self.r_id , user_id)

	def register_scorer(self):
		try:
			self.scorer=self.app_dict['MatchSupervisor'].match_scorer(self.match_id)
		except Exception as e:
			self.info('GI.register_scorer - Could not get Match scorer')
			pass

	def unregister_scorer(self):
		try:
			self.scorer=None
		except Exception as e:
			self.info('GI.register_scorer - Could not get Match scorer')
			pass


	def get_active_members(self):
		return self.r_dict['ActiveMembers']


	def get_members(self):
		return self.r_dict['members']

	def set_member_active(self, UserID):
		self.debug('GI.set_member_active - Triggered for user: %s' %UserID)
		try:
			if UserID not in self.r_dict['ActiveMembers']:
				self.debug('GI.set_member_active - 1')
				if UserID not in self.r_dict['members']:
					self.debug('GI.set_member_active - 2')
					self.app_dict['UserSupervisor'].add_user({'id':int(UserID)})
					self.debug('GI.set_member_active - 3')
				self.r_dict['ActiveMembers'].update({UserID:self.sys_time()})
				self.debug('GI.set_member_active - 4')
				self.update_leaderboard()
				self.debug('GI.set_member_active - 5')
			else:
				self.debug('GI.set_member_active - User exists')
		except Exception as e:
			self.error('GI.set_member_active - Could not set member active %s'%(str(UserID)))
			self.error(e)

	def set_member_idle(self, UserID):
		self.r_dict['ActiveMembers'].pop(UserID)


	#TODO : I think lookup in self.app_dict['UserSupervisor'] for last seen is not right
	# because here we are looking for active member in game not in our site.
	def check_member_status(self):
		self.r_dict['ActiveMembers']=[]
		for member in self.r_dict['members']:
			try:
				last_seen=self.app_dict['UserSupervisor'].last_seen(member)
				if last_seen < USER_IDLE_TIME:
					self.r_dict['ActiveMembers'].append(member)
				else:
					pass

			except Exception as e:
				self.error('GI.check_member_status - Could not retrieve heartbeat interval for user: %s' %smember)
				self.error(e)
	def end_quarter(self, q_id):
		self.sync_if_required()
		self.debug('GI.end_quarter  - Checking if game is over based on quarters')
		if len(self.r_dict['ActiveMembers']) > 0:
			if q_id not in self.r_dict['Quarters_played']:
				self.r_dict['Quarters_played'].append(q_id)
		if self.r_dict['gameplay_type'] == 'Quarters':
			if q_id in self.r_dict['game_length']:
				if len(self.r_dict['game_length']) == len(self.r_dict['Quarters_played']):
					self.debug('GI.end_quarter - Game length reached - terminating game')
					for _mem in self.r_dict['ActiveMembers']:
						self.disable_member(_mem)
					self.end_game()
				else:
					#come back and do some rollup - get Q by Q stats
					self.debug('GI.end_quarter - Game still active - number of quarters not reached')
			else:
				pass
		else:
			#Gameplay type is plays - no need to calculate
			pass

	#TODO : JT we are not using uid in this function - JT OK
	#Todo : Sachin depricate this function by below function
	def check_member_plays(self, u_id):
		#run if gametype is plays
		max_plays=self.r_dict['game_length'][0]
		try:
			for user in self.leaderboard:
				if self.leaderboard[u_id]['Num_calls']>=max_plays:
					self.debug('GI.check_member_length -  user %s met match play count, disabling' %(u_id))
					self.disable_member(u_id)
					return False
				else:
					pass
		except Exception as e:
			self.debug('GI.check_member_length -  Exception - %s' %(e))

	#REPLACES check_member_plays
    #TODO: Creating this function when someone reload the game page
	def can_user_play_game(self, u_id):
		play_type=self.r_dict['gameplay_type']
		errors = {}
		match = self.app_dict['MatchSupervisor'].match_obj(self.match_id)

		if match.CurrQuar == 1 and match.CurrEvent['values']['event_id'] == 1:
			#TODO Match is started so by default I am allowing for E=1 and Q1 
			# Till this time leaderboard is not set yet 
			return True, errors

		if play_type=='Plays':
			max_plays=self.r_dict['game_length'][0]
			if u_id not in self.leaderboard:
				self.debug("GI.can_user_play_game User not found in leaderboard user id %s"%u_id)
				return True, errors
			user_stat =  self.leaderboard[u_id]
			if 'Num_calls' in  user_stat and user_stat['Num_calls'] < max_plays: #max_plays
				return True, errors
			else:
				errors['error_msg'] = constants.GAME_ERROR['PLAY_LIMIT_REACH']
				self.disable_member(u_id, errors)

		elif play_type == 'Quarters':
			
			if match:
				if match.CurrQuar in self.r_dict['game_length']:
					return True, errors
				else:
					message = constants.GAME_ERROR['QUARTER_LIMIT_REACH']
					message = message.replace("<game_id>", str(self.r_id))
					errors['error_msg'] = message
					self.disable_member(u_id, errors)
		errors['error_msg'] = errors['error_msg'] if 'error_msg' in errors else "Something went wrong."
		return False, errors


	def disable_member(self, u_id, errors = {}):
		#disable user input - optional completion message
		channel = self.get_channel()
		response = {}
		response['disable_user'] = {'user_id' : u_id,}
		response['disable_user'].update(errors)
		self.app_dict['ChannelSupervisor'].broadcast_message(channel, response)
		self.debug('GI.disable_member -  Triggered for user : %s ' %(u_id))
		

	#release all objects - archive chat and game in DB
	def end_game(self, resdict=None):
		self.debug('GI.end_game - initiated')
		self.update_leaderboard()
		#Populate data here
		ms=self.app_dict['MatchSupervisor']	
		match = ms.match_obj(self.match_id)	
		
		#JT: Do we need to store match condition?
		condition = match.get_curr_conditions()
		_m = {#'home_team_abbr': match.home_team['team_abbr'],
			#'away_team_abbr': away_team['team_abbr'], 
			 'conditions' : condition['Conditions']

			}

		_end={'date':0, 'game_name':self.r_dict['game_name'], 'link_to_game':'as', 'match':_m, }		

		self.r_dict['game_stats'] = {
										
									 	'chat' : self.chat,
									 	'match_stats' : _m
									}
		self.r_dict['leaderboard'] = self.leaderboard

		#Sachin : Not saving active member in a game.
		for _mem in self.r_dict['ActiveMembers']:
			self.disable_member(_mem)

		self.r_dict['active'] = False 
		self.info("GI.end_game : before saving data")
		self.info(self.r_dict)
		self.info("GI.end_game : before saving data")
		self.persist()

		self.remove_channel()
		self.clean_game_data()
		self.unregister_scorer()
		#TODO: Need to implement based on user stat implementatio
		#self.unload_member()
		#self.scorer.register_game(game_id)

	def clean_game_data(self):
		self.debug('GI.clean_game_data - Triggered')
		self.debug("GI.clean_game_data - realse r_dic memory");
		del self.r_dict
		self.redis_delete()


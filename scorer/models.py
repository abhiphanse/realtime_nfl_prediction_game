from django.db import models
from fccommon.models import NFLMatch

class ScorerMatch(models.Model):
    ROLES = (
             ('TIMEKEEPER','Timekeeper'),
             ('SCOREKEEPER','Scorekeeper'),
            )
    scorer_id = models.IntegerField()
    match_id = models.CharField(max_length=30)
    role = models.CharField(max_length=15,choices=ROLES)
    approve = models. BooleanField(default=False)
    added_date = models.DateTimeField(auto_now_add = True)
    modified_date = models.DateTimeField(auto_now = True)
    start_date = models.DateTimeField(default = None)

    def __str__(self):
        match = NFLMatch.objects.get(pk=self.match_id)
        return '%s @ %s' % (match.home_team, match.away_team)

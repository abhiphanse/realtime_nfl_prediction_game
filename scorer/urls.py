from django.conf.urls import patterns, url

from scorer import views

urlpatterns = patterns('',
	url(r'^event/result$', views.submit_event_result, name='submit_event_result'),
	url(r'^timekeeper/(?P<match_id>\w+)$', views.timekeeper, name='timekeeper'),
	url(r'^scorekeeper/(?P<match_id>\w+)$', views.scorekeeper, name='scorekeeper'),
	url(r'^timekeeper/time/set/(?P<time>\d+)/(?P<match_id>\w+)$', views.time_set, name='time_set'),
	url(r'^timekeeper/time/add/(?P<time>\d+)/(?P<match_id>\w+)$', views.time_add, name='time_add'),
	url(r'^timekeeper/time/remove/(?P<time>\d+)/(?P<match_id>\w+)$', views.time_remove, name='time_remove'),
	url(r'^$', views.scorer_dashboard, name='scorer_dashboard'),
	url(r'^register_for_match$', views.register_for_match, name='register_for_match'),
	url(r'^start_match/(?P<match_id>\w+)$', views.start_match, name='start_match'),
	url(r'^scorekeeper/events/submit/(?P<match_id>\w+)$', views.scorekeeper_submit_events, name='scorekeeper_submit_events'),
	url(r'^set/condition/(?P<match_id>\w+)$', views.set_condition, name='set_condition'),
	url(r'^end/play/(?P<match_id>\w+)$', views.end_play, name='end_play'),
	url(r'^end_game$', views.end_game, name='end_game'),
	url(r'^end_quarter$', views.end_quarter, name='end_quarter'),
	url(r'^red_zone$', views.red_zone, name='red_zone'),
	url(r'^get_remaining_time$', views.get_remaining_time, name='get_remaining_time'),
	
)
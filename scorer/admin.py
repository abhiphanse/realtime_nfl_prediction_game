from django.contrib import admin
from django import forms
from django.contrib.auth.hashers import is_password_usable

from scorer.models import  ScorerMatch
from fcgame.models import GameUser
from fccommon.models import NFLMatch

class ScorerMatchAddForm(forms.ModelForm):
    class Meta:
        model = ScorerMatch
        widgets = {
            'scorer_id': forms.Select(),
            'match_id': forms.Select(),
        }

class ScorerMatchAddAdmin(admin.ModelAdmin):
    form = ScorerMatchAddForm

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'scorer_id':
            scorers = GameUser.objects.filter(role='R_SCORER')
            kwargs['widget'].choices = ((s.id,s.email) for s in scorers)
        if db_field.name == 'match_id':
            matchs = NFLMatch.objects.all()
            kwargs['widget'].choices = ((m.pk,m) for m in matchs)
        return super(ScorerMatchAddAdmin, self).formfield_for_dbfield(db_field, **kwargs)

admin.site.register(ScorerMatch, ScorerMatchAddAdmin)

class AddScorer(GameUser):
    class Meta:
        proxy = True
        verbose_name = 'Add scorer'

class AddScorerAdmin(admin.ModelAdmin):
    def queryset(self, request):
        return self.model.objects.filter(role = 'R_SCORER')

    def save_model(self, request, obj, form, change):
        password = obj.password
        if not is_password_usable(password):
            obj.set_password(password)
        obj.save()
  
admin.site.register(AddScorer, AddScorerAdmin)
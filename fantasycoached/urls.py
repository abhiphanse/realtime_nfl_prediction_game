from django.conf.urls import patterns, include, url
from django.contrib import admin

from django.conf import settings
from django.conf.urls.static import static
from allauth.account import views as allauthviews
from tastypie.api import Api

from fcgame.api import UserGameResource, GameUserResource
import fcgame
from fcgame.admin_views import send_emails
admin.autodiscover()

v1_api = Api(api_name='v1')
v1_api.register(UserGameResource())
v1_api.register(GameUserResource())

urlpatterns = patterns('',
    #URls realted to all aouth
    url(r'^logout/$', 'django.contrib.auth.views.logout',{'next_page': '/'}),
    url(r'^beta$', allauthviews.login),
    url(r'', include('allauth.urls')),
    url(r'^selectable/', include('selectable.urls')),
    url(r'^api/',include(v1_api.urls)),
    #Our local URLs import 
    url(r'^fccommon/', include('fccommon.urls', namespace="fccommon")),
    url(r'^fcgame/', include('fcgame.urls', namespace="fcgame")),
    url(r'^scorer/', include('scorer.urls', namespace="scorer")),
    url(r'^how_to/', fcgame.views.how_to, name="how_to"),
    url(r'^rules/', fcgame.views.rules, name="rules"),
    url(r'^terms_and_conditions/', fcgame.views.terms_and_conditions, name="terms_and_conditions"),
    url(r'^privacy_policy/', fcgame.views.privacy_policy, name="privacy_policy"),
    url(r'^contact_us/', fcgame.views.contact_us, name="contact_us"),
    url(r'^blog/', fcgame.views.blog, name="blog"),
    url(r'^$', fcgame.views.landing_page, name="landing_page"),
    url(r'^settings$', fcgame.views.settings, name='settings'),
    #url(r'^404$', fcgame.views.err404, name='err404'),
    #Admin URLs
    url(r'^admin/send_email/', send_emails, name='send_email' ),
    url(r'^admin/', include(admin.site.urls)),
    
) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

#handler404 = 'fcgame.views.err404'
import datetime
import decimal
from django.db.models.base import ModelState
import json
from datetime import datetime
import calendar


class DateTimeEncoder(json.JSONEncoder):
    def default(self, obj):
       if hasattr(obj, 'isoformat'):
           return obj.isoformat()
       elif isinstance(obj, decimal.Decimal):
           return float(obj)
       elif isinstance(obj, ModelState):
           return None
       else:
           return json.JSONEncoder.default(self, obj)

def str2bool(v):
  return v.lower() in ("yes", "true", "t", "1")

def get_timestamp_diff(t1,t2=None):
  if t2 == None:
    d = datetime.utcnow()
    t2 = calendar.timegm(d.utctimetuple())
  return t1-t2
    
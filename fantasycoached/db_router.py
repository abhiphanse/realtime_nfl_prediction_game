class MongoRouter(object):
    """
    A router to control all database operations on models in the
    auth application.
    """
    sql_models = [  'NFLTeam',
                    'NFLMatch',
                    'UserLeague',
                    'MatchEventState',
                    'UserPrediction',
                    'EventResult',
                    'LeagueInvite',
                    'UserGame',
                    'ScorerMatch',
                    'GameInvite'
                 ] 
    def db_for_read(self, model, **hints):
        """
        Attempts to read auth models go to auth_db.
        """
        if model.__name__ in self.sql_models:
            return 'mongo'
        return None

    def db_for_write(self, model, **hints):
        """
        Attempts to write auth models go to auth_db.
        """
        if model.__name__ in self.sql_models:
            return 'mongo'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        """
        Allow relations if a model in the auth app is involved.
        """
        if obj1.__class__ in self.sql_models or \
           obj2.__class__ in self.sql_models:
           return True
        return None

    def allow_migrate(self, db, model):
        """
        Make sure the auth app only appears in the 'auth_db'
        database.
        """
        if db == 'mongo':
            print "-----------------------------------"
            return None
        elif model.__name__ in self.sql_models:
            return False
        return None

    def allow_syncdb(self, db, model):
        if model.__name__ in self.sql_models:
            if db == 'default':
                return False
            else:
                return True
        else:
            if db == 'default':
                return True
            else:
                return False
from django.forms import ModelForm
from django import forms
from models import NFLTeam, MatchEventState, NFLMatch



class NFLMatchFilterForm(forms.Form):
    WEEK_CHOICES = (
                (1,1),
                (2,2),
                (3,3),
                (4,4),
                (10,"All"),
              )
    team = forms.ModelChoiceField(queryset=NFLTeam.objects.all().order_by('team_abbr'),empty_label="Select team")
    week = forms.ChoiceField(choices=WEEK_CHOICES)

class NFLTeamForm(ModelForm):
    class Meta:
        model = NFLTeam

class MatchEventStateForm(ModelForm):
    class Meta:
        model = MatchEventState
        field = ['match']
        exclude = ['event_notes']
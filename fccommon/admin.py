import logging
from bson.objectid import ObjectId

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django import forms

from fccommon.models import NFLTeam, MatchEventState, NFLMatch
from fcgame.models import UserGame, GameInvite
from scorer.models import ScorerMatch

logger = logging.getLogger('admin')

# Register your models here.
admin.site.register(NFLTeam)
admin.site.register(MatchEventState)

class NFLMatchAddForm(forms.ModelForm):
    
    def __init__(self, *args, **kwargs):
        super(NFLMatchAddForm, self).__init__(*args, **kwargs)
        self.fields['home_team'] = forms.ModelChoiceField(queryset=NFLTeam.objects.all())
        self.fields['away_team'] = forms.ModelChoiceField(queryset=NFLTeam.objects.all())
        self.fields['nfl_week'] = forms.ChoiceField(choices=((x,"Week %s"%x) for x in xrange(1,18)))
    
    def save(self, force_insert=False, force_update=False, commit=True):
        m = super(NFLMatchAddForm, self).save(commit=False)
        if commit:
            m.save()
        return m

    class Meta:
        model = NFLMatch
        exclude = ('match_results','games','tv_station')
    

class NFLMatchAddAdmin(admin.ModelAdmin):
    form = NFLMatchAddForm
    actions = ['delete_selected']

    def delete_selected(self, request, queryset):
        def delete_invite(game):
            game_invites=GameInvite.objects.raw_query({'game_id.id':ObjectId(game.id)})
            for invite in game_invites:
                invite.delete()
                logger.info("Deleted invite id: %s for game: %s" % (invite.pk,game.game_name))

        def delete_game(match):
            games = UserGame.objects.raw_query({'nfl_match_id.id':ObjectId(match.id)})
            for game in games:
                delete_invite(game)
                game.delete()
                logger.info("Deleted game: %s for match: %s" % (game.game_name,match))
                

        def delete_scorer_matches(match_id):
            score_regs = ScorerMatch.objects.filter(match_id=match_id)
            for s_reg in score_regs:
                s_reg.delete()
                logger.info("Deleted scorer match: %s for match id: %s" % (s_reg.id,match_id))


        for nfl_match in queryset:
            delete_game(nfl_match)
            delete_scorer_matches(nfl_match.id)
            nfl_match.delete()
            logger.info("Deleted match: %s, match id: %s" % (nfl_match, nfl_match.id))

    delete_selected.short_description = "Delete selected NFL Match and its related objects"

admin.site.register(NFLMatch, NFLMatchAddAdmin)
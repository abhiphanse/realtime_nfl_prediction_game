#-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
'''
Most of these URLs are left over from development, i have commented a few out where needed, 
    but it should provide a good idea of what we're working with
'''


from django.conf.urls import patterns, url

from fccommon import views

urlpatterns = patterns('',
    url(r'^nflteam/add/$', views.nfl_team_add, name='nfl_team_add'),
    url(r'^dummy', views.dummy),
    url(r'^nflteam/view/$', views.nfl_team_view, name='nfl_team_view'),
    url(r'^nflteam/sync/$', views.nfl_team_sync, name='nfl_team_sync'),
    url(r'^nflmatch/view/$', views.nfl_match_view, name='nfl_match_view'),
    url(r'^nflmatch/view/(?P<week>\d+)/$', views.nfl_match_view, name='nfl_match_view'),
    url(r'^nflmatch/view/(?P<team>\w+)/$', views.nfl_match_view, name='nfl_match_view'),
    url(r'^nflmatch/view/(?P<team>\w+)/(?P<week>\d+)/$', views.nfl_match_view, name='nfl_match_view'),
    url(r'^nflmatch/sync/$', views.nfl_match_sync, name='nfl_match_sync'),   
)

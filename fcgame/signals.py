from django.db.models.signals import post_save
from fccommon.models import NFLMatch
from fcgame.models import UserGame

def update_match(sender, **kw):
    game_obj = kw["instance"]
    if kw["created"]:
        match_obj = NFLMatch.objects.get(pk=game_obj.nfl_match_id.pk)
        if game_obj.id not in match_obj.games:
            match_obj.games.append(game_obj.id)
            match_obj.games_count += 1 
            match_obj.save()


post_save.connect(update_match, sender=UserGame)
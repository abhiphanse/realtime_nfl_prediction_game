from django.conf.urls import patterns, url

from fcgame import views

urlpatterns = patterns('',
	url(r'^game/create$', views.create_game, name='create_game'),
	url(r'^game/list$', views.game_list, name='game_list'),
	url(r'^game/play/(?P<game_id>\w+)$', views.game_play, name='game_play'),
	url(r'^game/(?P<game_id>\w+)/leaderboard/$', views.game_leaderboard, name='game_leaderboard'),
	url(r'^game/(?P<game_id>\w+)/prediction/$', views.user_prediction, name='user_prediction'),

	url(r'^league/create$', views.create_league, name='create_league'),
	url(r'^user/view$', views.user_profile, name='user_profile'),
	url(r'^play/anonymous$', views.play_anonymous, name='play_anonymous'),
	url(r'^user_dashboard$', views.user_dashboard, name='user_dashboard'),
	#url(r'^user/predict$', views.user_prediction, name='user_prediction'),
	url(r'^accept_league_invite/(?P<invite_id>\w+)/$', views.accept_league_invite, name='accept_league_invite'),
	#url(r'^update_invitation/(?P<invite_id>\w+)/(?P<invite_id>\w+)/$', views.update_invitation, name='accept_league_invite'),
	url(r'^invites_notifications$', views.invites_notifications, name='invites_notifications'),
	url(r'^join_game$', views.join_game, name='join_game'),
	url(r'^single_game_profile/(?P<game_id>\w+)$', views.single_game_profile, name='single_game_profile'),
	url(r'^league/profile$', views.league_profile, name='league_profile'),
	url(r'^league/game_profile$', views.league_game_profile, name='league_game_profile'),
	url(r'^games$', views.my_games, name='my_games'),
	url(r'^accept_invite/(?P<invite_type>\d+)/(?P<invite_id>\w+)/$', views.update_invitation,{'accept':True}, name='accept_invite'),
	url(r'^decline_invite/(?P<invite_type>\d+)/(?P<invite_id>\w+)/$', views.update_invitation,{'accept':False}, name='decline_invite'),	
	url(r'^game/chat/(?P<game_id>\w+)$', views.game_chat, name='game_chat'),
	url(r'^join_game_profile/(?P<game_id>\w+)$', views.join_game_profile, name='join_game_profile'),
	url(r'^game/join/(?P<game_id>\w+)$', views.game_join, name='game_join'),
	url(r'^game/load_chat/(?P<game_id>\w+)$', views.load_chat, name='load_chat'),
	
)
from django.contrib import admin

from fcgame.models import  GameUser, UserLeague, UserGame
from fccommon.models import NFLMatch
from fcgame.lookups import GameLookup

from django import forms
from selectable.forms import AutoCompleteWidget, AutoCompleteSelectField, AutoCompleteSelectMultipleField


# Register your models here.
admin.site.register(GameUser)
class UserLeagueAdmin(admin.ModelAdmin):
    model = UserLeague
    exclude  = ('members','created_by')
    def save_model(self, request, obj, form, change):
        obj.created_by = request.user.id
        obj.save()

admin.site.register(UserLeague,UserLeagueAdmin)


class PandingSignup(GameUser):
    class Meta:
        ordering = ['added_date']
        proxy = True
        verbose_name = 'Panding signup user'

class SignupAdmin(admin.ModelAdmin):
    def queryset(self, request):
        return self.model.objects.filter(is_active = False)

admin.site.register(PandingSignup, SignupAdmin)

class UserGameForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(UserGameForm, self).__init__(*args, **kwargs)
        self.fields['nfl_match_id'] = forms.ModelChoiceField(queryset=NFLMatch.objects.all())
        self.fields['members'].initial = kwargs['instance'].members
        
    def save(self, force_insert=False, force_update=False, commit=True):
        user_game = super(UserGameForm, self).save(commit=False)
        nfl_match_id = user_game.nfl_match_id
        members = user_game.members
        m = []
        for member in members:
            m.append(member.id)
        user_game.members = m
        user_game.nfl_match_id = NFLMatch.objects.get(pk=nfl_match_id.id)
        if commit:
            user_game.save()
        return user_game

    members = AutoCompleteSelectMultipleField(lookup_class=GameLookup,required=False)
    class Meta:
        model = UserGame
        exclude = ('game_length',)
        

class UserGameAdmin(admin.ModelAdmin):
    form = UserGameForm

admin.site.register(UserGame, UserGameAdmin)
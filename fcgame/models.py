from django.db import models
from django.db.models.signals import post_save
from djangotoolbox.fields import ListField, EmbeddedModelField, DictField
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django_mongodb_engine.contrib import MongoDBManager
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager

from fccommon.models import NFLTeam, NFLMatch, MatchEventState, CategoryField, DictOverrideField


# Abstract class for user 
class MyUserManager(BaseUserManager):
    def create_user(self, email, password=None):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=MyUserManager.normalize_email(email),
            
        )

        user.set_password(password)
        user.is_active = True
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(email,
            password=password,
            
        )
        user.is_admin = True
        user.is_active = True
        user.save(using=self._db)
        return user


class GameUser(AbstractBaseUser):
    AUTH_METHODS = (
                    ('AM_FACEBOOK','Facebook'),
                    ('AM_GOOGLE','Google'),
                    ('AM_EMAIL','Email')
                   )
    ROLES = (
             ('R_PLAYER','Player'),
             ('R_ADMIN','Admin'),
             ('R_STAFF','Staff'),
             ('R_SCORER','Scorer')
            )
    IS_ACTIVES = (
                   ('PENDING',0),
                   ('ACTIVE',1),
                   ('BLOCK',2)
                 )
    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
        db_index=True,
    )
    username = models.CharField(max_length=30)
    name = models.CharField(max_length=30,blank=True)
    auth_method = models.CharField(max_length=10,choices=AUTH_METHODS,blank=True)
    favorite_team = models.CharField(max_length=50,blank=True)
    notes = models.TextField(blank=True)
    role = models.CharField(max_length=10,choices=ROLES,blank=True)
    is_active = models.BooleanField(default=False)
    added_date = models.DateTimeField(auto_now_add = True)
    modified_date = models.DateTimeField(auto_now = True)
    is_admin = models.BooleanField(default=False)
    profile_picture = models.ImageField(upload_to='user/pictures/',blank=True)
    
    objects = MyUserManager()

    USERNAME_FIELD = 'email'
    

    def get_full_name(self):
        # The user is identified by their email address
        return self.email

    def get_short_name(self):
        # The user is identified by their email address
        return self.email

    def __str__(self):
        return "Email: %s, Username: %s" % (self.email, self.username)

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin
 

class UserLeague(models.Model):
   
    name = models.CharField(max_length=30)
    members = CategoryField()
    created_by = models.IntegerField()
    added_date = models.DateTimeField(auto_now_add = True)
    modified_date = models.DateTimeField(auto_now = True)
    start_date = models.DateTimeField() 
    end_date = models.DateTimeField()
    is_private = models.BooleanField()
    team_tagline = models.CharField(max_length=60)


class UserPrediction(models.Model):
    nfl_match_id = EmbeddedModelField(NFLMatch)
    event_id = EmbeddedModelField(MatchEventState)
    user_id = models.IntegerField()
    user_prediction = ListField()
    prediction_type = models.CharField(max_length=30)
    added_date = models.DateTimeField(auto_now_add = True)
    modified_date = models.DateTimeField(auto_now = True)


class LeagueInvite(models.Model):
    invite_id = models.AutoField(primary_key=True)
    league_id = EmbeddedModelField(UserLeague)
    email = models.EmailField(max_length=20)
    invitee_user_id = models.IntegerField()
    accepted = models.BooleanField(default=False)
    added_date = models.DateTimeField(auto_now_add = True)
    modified_date = models.DateTimeField(auto_now = True)
    is_read = models.BooleanField(default=False)


class UserGame(models.Model):
    GAME_PLAY_TYPES = (
                   ('Quarters','Quarters'),
                   ('Plays','Plays'),
                 )
    nfl_match_id = EmbeddedModelField(NFLMatch)
    gameplay_type = models.CharField(max_length=10,choices=GAME_PLAY_TYPES)
    game_length = CategoryField()
    league_game = models.BooleanField()
    game_name = models.CharField(max_length=30)
    organizer = models.IntegerField()
    members = CategoryField(blank=True)
    game_stats = DictOverrideField(blank=True)
    leaderboard = DictOverrideField(blank=True)
    added_date = models.DateTimeField(auto_now_add = True)
    modified_date = models.DateTimeField(auto_now = True)
    is_private = models.BooleanField()
    objects = MongoDBManager()

    def __str__(self):
        return "Game Name: %s"%(self.game_name)

class GameInvite(models.Model):
    invite_id = models.AutoField(primary_key=True)
    game_id = EmbeddedModelField(UserGame)
    email = models.EmailField(max_length=20)
    invitee_user_id = models.IntegerField()
    accepted = models.BooleanField(default=False)
    added_date = models.DateTimeField(auto_now_add = True)
    modified_date = models.DateTimeField(auto_now = True)
    is_read = models.BooleanField(default=False)
    objects = MongoDBManager()

from south.modelsinspector import add_introspection_rules
add_introspection_rules([], ["^djangotoolbox\.fields\.ListField"])
add_introspection_rules([], ["^djangotoolbox\.fields\.EmbeddedModelField"])
add_introspection_rules([], ["^djangotoolbox\.fields\.DictField"])
add_introspection_rules([], ["^fccommon\.models\.CategoryField"])
#djangotoolbox.fields.ListField



def update_match(sender, **kw):
    
    game_obj = kw["instance"]
    if kw["created"]:
        match_obj = NFLMatch.objects.get(pk=game_obj.nfl_match_id.pk)
        
        if game_obj.id not in match_obj.games:
            match_obj.games.append(game_obj.id)
            match_obj.games_count += 1 
            match_obj.save()
        


post_save.connect(update_match, sender=UserGame)